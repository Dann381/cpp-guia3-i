# Guía 3 - I

Los archivos contenidos en este repositorio contienen 3 ejercicios de c++ que están descritos en el pdf adjunto en el mismo repositorio. A continuación se dará una breve explicación de cada ejercicio.

### Ejercicio 1
Este ejercicio consiste en la creación de una lista desordenada de números ingresados por el usuario, la cual será dividida en dos listas independientes ordenadas ascendentemente, una formada por los números positivos y otra por los números negativos.

### Ejercicio 2
Este ejercicio consiste en la creación de una lista ordenada con nombres (string) ingresados por el usuario. Por cada ingreso, se muestra el contenido de la lista, además de mostrar un pequeño menú para agregar, imprimir datos o terminar el programa.

### Ejercicio 3
Este último ejercicio consiste en la creación de una lista ordenada alfabeticamente de postres. Cada postre contiene una lista de sus ingredientes. Dentro del programa existen opciones para agregar o eliminar postres, ver los postres existentes, seleccionar un postre para: ver, agregar o eliminar ingredientes, y terminar el programa.

### Ejecución
Cada ejercicio contiene su propio makefile. Para compilar cada ejercicio, es necesario entrar en su respectiva carpeta, y escribir en la terminal el comando "make". Esto dejará un archivo con el nombre del ejercicio, el cuál se puede ejecutar escribiendo el comando "./". Ejemplo: Si usted desea ejecutar el ejercicio 1, entre en la carpeta del ejercicio 1 mediante la terminal (use el comando "cd" para desplazarse por las carpetas), luego escriba el comando "make", y a continuación escriba el comando "./ejercicio1".

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
