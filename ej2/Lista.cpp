
#include <iostream>
using namespace std;
#include "Lista.h"

Lista::Lista(){}

void Lista::agregar(string nombre){
    Nodo *q;

    q = new Nodo;
    q->nombre = nombre;
    q->sig = NULL;

    if(this->p == NULL){
        this->p = q;
    }
    else {
        Nodo *s, *r;

        s = this->p;
        while ((s != NULL) && (s->nombre < nombre)){
            r = s;
            s = s->sig;
        }
        if (s == this->p){
        this->p = q;
        this->p->sig = s;
        }
        else{
            r->sig = q;
            q->sig = s;
        }
    }
}

void Lista::imprimir(){

    Nodo *q = this->p;
    cout << "Nombres:\n";
    while (q != NULL){
        cout << q->nombre << "\n";
        q = q->sig;
    }
    cout << endl;
}