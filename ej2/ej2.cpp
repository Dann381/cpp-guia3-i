#include <iostream>
using namespace std;
#include "Lista.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

void crear_lista(Lista *lista){
    int opc = 0;
    string line;
    
    do{ cout << "Menú" << endl;
        cout << "--------------------------" << endl;
        cout << "[1] Agregar" << endl;
        cout << "[2] Imprimir" << endl;
        cout << "[3] Terminar" << endl;
        cout << "--------------------------" << endl;
        cout << "Opción: ";
        cin >> opc;

        switch (opc)
        {
        case 1:
            Clear();
            cout << "Nombre: ";
            cin.ignore();
            getline(cin, line);
            lista->agregar(line);
            lista->imprimir();
            break;
        case 2:
            Clear();
            lista->imprimir();
            break;
        case 3:
            Clear();
            break;
        default:
            break;
        }
    }
    while(opc != 3);
}

int main(){
    Clear();
    Lista *lista = new Lista();
    crear_lista(lista);
}
