#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    string nombre;
    struct _Nodo *sig;
} Nodo;

class Lista{
    public:
        Nodo *p = NULL;
        Lista();
        void agregar(string nombre);
        void imprimir();
};
#endif