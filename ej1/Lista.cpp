#include <iostream>
using namespace std;
#include "Lista.h"

Lista::Lista(){}

void Lista::agregar(int numero){
    Nodo *q;

    q = new Nodo;
    q->numero = numero;
    q->sig = NULL;

    if(this->p == NULL){
        this->p = q;
    }
    else {
        Nodo *s, *r;

        s = this->p;
        this->p = q;
        this->p->sig = s;
    }
}

void Lista::imprimir(){

    Nodo *q = this->p;
    cout << "Números: ";
    while (q != NULL){
        cout << q->numero << " ";
        q = q->sig;
    }
    cout << endl;
}

void Lista::ordenar(int numero){
    Nodo *q;

    q = new Nodo;
    q->numero = numero;
    q->sig = NULL;

    if(this->p == NULL){
        this->p = q;
    }
    else {
        Nodo *s, *r;

        s = this->p;
        while ((s != NULL) && (s->numero < numero)){
            r = s;
            s = s->sig;
        }
        if (s == this->p){
            this->p = q;
            this->p->sig = s;
        }
        else{
            r->sig = q;
            q->sig = s;
        }
    }
}
