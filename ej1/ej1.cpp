#include <iostream>
using namespace std;

#include "Lista.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

int main(){
    Clear();
    int opc = 0;
    string line;
    Lista *lista = new Lista();
    Lista *lista_mas = new Lista();
    Lista *lista_menos = new Lista();
    
    do{ 
        cout << "--------------------------" << endl;
        cout << "[1] Agregar" << endl;
        cout << "[2] Imprimir" << endl;
        cout << "[3] Finalizar agregado" << endl;
        cout << "--------------------------" << endl;
        cout << "Opción: ";
        cin >> opc;

        switch (opc)
        {
        case 1:
            Clear();
            cout << "Número: ";
            cin >> line;
            lista->agregar(stoi(line));
            break;
        case 2:
            Clear();
            lista->imprimir();
            break;
        case 3:
            Clear();
            cout << "Lista creada" << endl;
            break;
        default:
            break;
        }
    }
    while(opc != 3);

    Nodo *q = lista->p;
    while (q != NULL){
        if (q->numero >= 0){
            lista_mas->ordenar(q->numero);
        }
        else {
            lista_menos->ordenar(q->numero);
        }
        q = q->sig;
    }
    lista_mas->imprimir();
    lista_menos->imprimir();

    delete lista;
    delete lista_mas;
    delete lista_menos;
    
    return 0;
}